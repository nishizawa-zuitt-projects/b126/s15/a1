// #1
function numbers(number1, number2){
	if(number1 > number2){
		console.log(`${number1} is higher than ${number2}`)
	}else if(number1 < number2){
		console.log(`${number2} is higher than ${number1}`)
	}else if(number1 === number2){
		console.log("They are the same")
	}
}


// #2
function pickLanguage(language){
	switch(language){
		case "French":
			return "Bonjour"
			break
		case "Spanish":
			return "Hola"
			break
		case "Japanese":
			return "こんにちは"
			break
		default:
			return "Please input a different language."
	}
}


// #3
function count(singular, number){
	if((singular[0] == 'A' || singular[0] == 'a'
      || singular[0] == 'E' || singular[0] == 'e'
      || singular[0] == 'I' || singular[0] == 'i'
      || singular[0] == 'O' || singular[0] == 'o'
      || singular[0] == 'U' || singular[0] == 'u')
      && number == 1){

		console.log(`I have an ${singular}.`)

	}else if(!(singular[0] == 'A' || singular[0] == 'a'
      || singular[0] == 'E' || singular[0] == 'e'
      || singular[0] == 'I' || singular[0] == 'i'
      || singular[0] == 'O' || singular[0] == 'o'
      || singular[0] == 'U' || singular[0] == 'u')
      && number == 1){

		console.log(`I have a ${singular}.`)

	}else if(number > 1){

		console.log(`I have ${number} ${singular}s.`)

	}else if((singular[0] == 'A' || singular[0] == 'a'
      || singular[0] == 'E' || singular[0] == 'e'
      || singular[0] == 'I' || singular[0] == 'i'
      || singular[0] == 'O' || singular[0] == 'o'
      || singular[0] == 'U' || singular[0] == 'u')
      && number > 1){

		console.log(`I have ${number} ${singular}.`)

	}else if(typeof number === "string"){
		console.log("Invalid data type. Please enter a number.")
	}
}

// #4
function grade(gradeNumber){
	if(gradeNumber < 60){

		console.log("Your grade is F")

	}else if (gradeNumber <= 69 && gradeNumber >= 60){

		console.log("Your grade is D")

	}else if (gradeNumber <= 79 && gradeNumber >= 70){

		console.log("Your grade is C")

	}else if (gradeNumber <= 89 && gradeNumber >= 80){

		console.log("Your grade is B")
		
	}else if (gradeNumber <= 100 && gradeNumber >= 90){

		console.log("Your grade is A")
		
	}else if (gradeNumber > 100 || gradeNumber < 0){

		console.log("Please enter a number within 0 to 100")
		
	}
}

// #5
function calculator(num1, num2, symbol){
	switch(symbol){
		case "+":
			return num1 + num2
			break
		case "-":
			return num1 - num2
			break
		case "*":
			return num1 * num2
			break
		case "/":
			return num1 / num2
			break
		default:
			return "You entered an unknown symbol"
			break
	}
}